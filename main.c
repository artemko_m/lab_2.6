#include<windows.h>
#include<math.h>
#include<stdio.h>
#include <stdbool.h>
#include <limits.h>
#include <conio.h>
#define N 11

double** createEmptyMatrix(int n){
    double** emptyMatrix = (double**)malloc(n * sizeof(double*));
    for (int i = 0; i < n; i++){
        emptyMatrix[i] = (double*)malloc(n * sizeof(double));
    }

    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            emptyMatrix[i][j] = 0;
        }
    }

    return emptyMatrix;
}

double* createEmptyArray(int n){
    double* array = (double*)malloc(n * sizeof(double*));
    for(int i = 0; i < n; i++){
        array[i] = 0;
    }

    return array;
}

double** copyMatrix(double **matrix){
    double** newMatrix = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            newMatrix[i][j] = matrix[i][j];
        }
    }

    return newMatrix;
}

double** randmm(int rows, int cols)
{
    double** matrix = createEmptyMatrix(N);
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            matrix[i][j] =  (double)rand()/(double)(RAND_MAX/2.0);
        }
    }
    return matrix;
}

double** mulmr(double num, double **mat, int n)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            mat[i][j] = mat[i][j] * num;
            if(mat[i][j] > 1.0)
            {
                mat[i][j] = 1;
            }
            else mat[i][j] = 0;
        }
    }
    return mat;
}

double** multiplyByNumber(double** matrix, double number){
    double** multipliedMatrix = createEmptyMatrix(N);

    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            multipliedMatrix[i][j] = matrix[i][j] * number;
        }
    }

    return multipliedMatrix;
}

double** elementMultiplication(double **matrix1, double **matrix2){
    double** resultMatrix = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            resultMatrix[i][j] = matrix1[i][j] * matrix2[i][j];
        }
    }

    return resultMatrix;
}

double** elementSum(double **matrix1, double **matrix2){
    double** result = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            result[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }

    return result;
}

void printMatrix(double **matrix)
{
    for(int i = 0; i<N; i++)
    {
        for(int j = 0; j<N; j++)
        {
            printf("%g  ", matrix[i][j]);
        }
        printf("\n");
    }
}

double** symmetrize(double **matrix){
    double** result = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            result[i][j] = matrix[i][j];
        }
    }

    for(int i = 0; i<N; i++){
        for(int j = 0; j<N; j++){
            result[j][i] = result[i][j];
        }
    }

    return result;
}

double** roundm(double **matrix){
    double** roundedMatrix = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            double element = matrix[i][j];
            roundedMatrix[i][j] = round(element);
        }
    }
    return roundedMatrix;
}

double** getWt(double **A){
    double** randomMatrix = randmm(N, N);
    double** multipliedByNumberMatrix = multiplyByNumber(randomMatrix, 100);
    double** multipliedMatrices = elementMultiplication(multipliedByNumberMatrix, A);
    double** WtMatrix = roundm(multipliedMatrices);
    return WtMatrix;
}

double** getB(double **WtMatrix){
    double** result = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if(WtMatrix[i][j] > 0){
                result[i][j] = 1;
            }
            if(WtMatrix[i][j] == 0){
                result[i][j] = 0;
            }
        }
    }

    return result;
}

double** getC(double **BMatrix){
    double** result = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if(BMatrix[i][j] != BMatrix[j][i]){
                result[i][j] = 1;
            } else {
                result[i][j] = 0;
            }
        }
    }

    return result;
}

double** getD(double **BMatrix){
    double** result = createEmptyMatrix(N);
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if(BMatrix[i][j] == BMatrix[j][i] == 1){
                result[i][j] = 1;
            } else {
                result[i][j] = 0;
            }
        }
    }

    return result;
}

double** changeWtMatrix(double **WtMatrix, double **CMatrix, double **DMatrix){
    double** Tr = createEmptyMatrix(N);

    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if(i < j){
                Tr[i][j] = 1;
            } else {
                Tr[i][j] = 0;
            }
        }
    }

    double** multipliedD_Tr = elementMultiplication(DMatrix, Tr);
    double** summedC_DTr = elementSum(CMatrix, multipliedD_Tr);
    double** newWt = elementMultiplication(summedC_DTr, WtMatrix);

    return newWt;
}

struct stack {
    int array[N];
    int top;
};

struct stack* initStack() {
    struct stack* stack = malloc(sizeof(struct stack));
    stack->top = 0;
    return stack;
}

void push(struct stack* stack, int value) {
    if (stack->top < N) {
        stack->array[stack->top] = value;
        stack->top++;
    }
}

void pop(struct stack* stack) {
    stack->top--;
}

int top(struct stack* stack) {
    if (stack->top > 0)
        return stack->array[stack->top - 1];
    else return -1;
}

int isEmptyStack(struct stack* stack) {
    if (stack->top == 0)
        return 1;
    else
        return 0;
}

int DFS(double **matrix, int start, int end) {
    double** DFS_tree = createEmptyMatrix(N);
    struct stack* s = initStack();
    double* visited = createEmptyArray(N);
    int currentVertex;
    push(s, start);
    visited[start] = 1;
    while (!isEmptyStack(s)) {
        currentVertex = top(s);
        for (int i = 0; i < N; i++) {
            if (matrix[currentVertex][i]) {
                if (i == end) return 1;
                if (visited[i] == 0) {
                    visited[i] = 1;
                    DFS_tree[currentVertex][i] = 1;
                    push(s, i);
                    break;
                }
            }
            if (i == N - 1) {
                pop(s);
            }
        }
    }
    return 0;
}

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

char ProgName[]="Лабораторна робота 6";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
    HWND hWnd;
    MSG lpMsg;
    WNDCLASS w;
    w.lpszClassName=ProgName;
    w.hInstance=hInstance;
    w.lpfnWndProc=WndProc;
    w.hCursor=LoadCursor(NULL, IDC_ARROW);
    w.hIcon=0;
    w.lpszMenuName=0;
    w.hbrBackground = LTGRAY_BRUSH;
    w.style=CS_HREDRAW|CS_VREDRAW;
    w.cbClsExtra=0;
    w.cbWndExtra=0;

    if(!RegisterClass(&w))
        return 0;

    hWnd=CreateWindow(ProgName,
                      "Лабораторна робота 6. Виконав Матюшенко А.В.",
                      WS_OVERLAPPEDWINDOW,
                      0,
                      0,
                      1080,
                      740,
                      (HWND)NULL,
                      (HMENU)NULL,
                      (HINSTANCE)hInstance,
                      (HINSTANCE)NULL);

    ShowWindow(hWnd, nCmdShow);

    while(GetMessage(&lpMsg, hWnd, 0, 0))
    {
        TranslateMessage(&lpMsg);
        DispatchMessage(&lpMsg);
    }
    return(lpMsg.wParam);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT messg,
                         WPARAM wParam, LPARAM lParam)
{
    HDC hdc;
    PAINTSTRUCT ps;

    switch(messg)
    {
    case WM_PAINT :
        hdc=BeginPaint(hWnd, &ps);

        int n1 = 0;
        int n2 = 4;
        int n3 = 1;
        int n4 = 6;
        int n = 10 + n3;
        srand(0416);
        double** T = randmm(n,n);
        double** A = mulmr((1.0 - n3*0.01 - n4*0.005 - 0.05), T, n);
        double** Wt = getWt(A);
        double** B = getB(Wt);
        double** C = getC(B);
        double** D = getD(B);
        double** newWt = changeWtMatrix(Wt, C, D);
        Wt = symmetrize(newWt);
        double** symmetricalMatrix = symmetrize(A);

        for(int i = 0; i<N; i++){
            for(int j = 0; j < N; j++){
                if(Wt[i][j] > 0 && i <= j){
                    printf("Edge %d - %d:  %g\n", i+1, j+1, Wt[i][j]);
                }
            }
        }

        printf("\n");

        char *nn[13] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
        int nx[15] = {};
        int ny[15] = {};
        int dx = 16, dy = 16, dtx = 5;
        int radius = 300;
        int offsetX = 500;
        int offsetY = 350;

        //Create coordinates
        float degree = (360/(n-1))*(M_PI/180);
        for(int i = 0; i<n; i++)
        {
            nx[i] = offsetX + radius * cos(degree*i);
            ny[i] = offsetY + radius * sin(degree*i); //0.628319
        }

        //Center coordinates
        nx[n-1] = offsetX;
        ny[n-1] = offsetY;


        HPEN BPen = CreatePen(PS_SOLID, 2, RGB(50, 0, 255));
        HPEN KPen = CreatePen(PS_SOLID, 1, RGB(20, 20, 5));
        HPEN YPen = CreatePen(PS_SOLID, 2, RGB(255, 236, 66));
        HPEN GPen = CreatePen(PS_SOLID, 2, RGB(88, 235, 52));

        void arrow(float fi, int px, int py)
        {
            px += dx*cos(fi);
            py += dx*sin(fi);
            int lx,ly,rx,ry;
            lx = px+dx*cos(fi+0.26);
            rx = px+dx*cos(fi-0.26);
            ly = py+dx*sin(fi+0.26);
            ry = py+dx*sin(fi-0.26);
            MoveToEx(hdc, lx, ly, NULL);
            LineTo(hdc, px, py);
            LineTo(hdc, rx, ry);
        }

        double getAngle(int nxi, int nyi, int nxj, int nyj)
        {
            double angle = M_PI + acos((nxj-nxi)/(sqrt(pow((nxj-nxi),2)+pow((nyj-nyi),2))));
            if(nyj < nyi) angle *= -1;
            return angle;
        }

        void arrowPolyline(int fromX, int fromY, int toX, int toY)
        {
            double angle = getAngle(fromX, fromY, toX, toY);
            arrow(angle, toX, toY);
        }

        void polyline(int nxi, int nyi, int nxj, int nyj)
        {
            if((nxi > offsetX && nyi < offsetY) && (nxj < offsetX && nyj > offsetY))  //I-III
            {
                LineTo(hdc, offsetX+2*dx, offsetY+dy);
                LineTo(hdc, nxj, nyj);
            }
            else if((nxi < offsetX && nyi > offsetY) && (nxj > offsetX && nyj < offsetY))   //III-I
            {
                LineTo(hdc, offsetX-2*dx, offsetY-dy);
                LineTo(hdc, nxj-dx*0.2, nyj);
            }
            else if((nxi > offsetX && nyi > offsetY) && (nxj < offsetX && nyj < offsetY))   //IV-II
            {
                LineTo(hdc, offsetX+3*dx, offsetY);
                LineTo(hdc, nxj, nyj);
            }
            else if ((nxi < offsetX && nyi < offsetY) && (nxj > offsetX && nyj > offsetY))   //II-IV
            {
                LineTo(hdc, offsetX-3*dx, offsetY-dy);
                LineTo(hdc, nxj, nyj);
            }
            else if(nyi == offsetY)    //vertical vertices
            {
                LineTo(hdc, offsetX, offsetY-2*dy);
                LineTo(hdc, nxj, nyj);
            }
            else if(nyj == offsetY)    //vertical vertices
            {
                LineTo(hdc, offsetX, offsetY+2*dy);
                LineTo(hdc, nxj, nyj);
            }
            else if(nxi == offsetX)    //horizontal vertices
            {
                LineTo(hdc, offsetX-2*dx, offsetY);
                LineTo(hdc, nxj, nyj);
            }
            else if(nxj == offsetX)    //horizontal vertices
            {
                LineTo(hdc, offsetX-4*dx, offsetY);
                LineTo(hdc, nxj-dx*0.3, nyj);
            }
        }

        void drawLoop(int nxi, int nyi, int nxj, int nyj)
        {
            if(nyi > offsetY)
            {
                Arc(hdc, nxj-5, nyj+5, nxj+40, nyj+50,    nxj, nyj, nxj, nyj);
            }
            else
            {
                Arc(hdc, nxj+5, nyj-5, nxj-40, nyj-50,    nxj, nyj, nxj, nyj);
            }
        }

        void parallelLine(double angle, int nxi, int nyi, int nxj, int nyj)
        {
            MoveToEx(hdc, nxi-dx*cos(angle-M_PI/16), nyi-dy*sin(angle-M_PI/16), NULL);
            LineTo(hdc, nxj+dx*cos(angle+M_PI/16), nyj+dy*sin(angle+M_PI/16));
            arrow(angle, (nxj+dx*cos(angle+M_PI/16))-16*cos(angle), (nyj+dy*sin(angle+M_PI/16))-16*sin(angle));
        }

        SelectObject(hdc, KPen);


        int loopCheck(int start, int end, double **visited, double **A)
        {
            if (start == end) return 1;
            if (visited[start] && visited[end]) return 1;
            return 0;
        }

        int totalWeight = 0;
        double** MSTMatrix = createEmptyMatrix(N);

        void MST_Kruskal(double **W){
            int visitedEdges = 0, start, end;
            double** visitedVertex = createEmptyArray(N);
            while(visitedEdges < N-1){
                int currentWeight = 200;
                for (int i = 0; i < N; i++) {
                    for (int j = i; j < N; j++) {
                        if (W[i][j] && W[i][j] < currentWeight) {
                            currentWeight = W[i][j];
                            start = i;
                            end = j;
                        }
                    }
                }
                if (loopCheck(start, end, visitedVertex,MSTMatrix) && DFS(MSTMatrix,start,end)) {
                    W[start][end] = 0;
                }
                else {
                totalWeight += currentWeight;
                MSTMatrix[start][end] = currentWeight;
                MSTMatrix[end][start] = currentWeight;
                W[start][end] = 0;
                visitedVertex[start] = 1;
                visitedVertex[end] = 1;
                visitedEdges++;

                sleep(1);

                SelectObject(hdc, GPen);
                if((start-end ==(n-1)/2 || end-start ==(n-1)/2) && start != n-1 && end != n-1 && n%2 == 1)
                {
                    MoveToEx(hdc, nx[start], ny[start], NULL);
                    polyline(nx[start], ny[start], nx[end], ny[end]);
                }
                else
                {
                    MoveToEx(hdc, nx[start], ny[start], NULL);
                    LineTo(hdc, nx[end], ny[end]);
                }

                SelectObject(hdc, YPen);
                Ellipse(hdc, nx[start]-dx, ny[start]-dy, nx[start]+dx, ny[start]+dy);
                TextOut(hdc, nx[start]-dtx, ny[start]-dy/2, nn[start], 2);
                Ellipse(hdc, nx[end]-dx, ny[end]-dy, nx[end]+dx, ny[end]+dy);
                TextOut(hdc, nx[end]-dtx, ny[end]-dy/2, nn[end], 2);
                }
            }
        }

        //Draw graph
        for(int i = 0; i<n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(symmetricalMatrix[i][j] == 1 && i <= j)
                {
                    int xMiddle = (nx[i] + nx[j])/2;
                    int yMiddle = (ny[i] + ny[j])/2;
                    char str[10];
                    sprintf(str, "%g", Wt[i][j]);
                    if(i == j)
                    {
                        drawLoop(nx[i], ny[i], nx[j], ny[j]);
                    }
                    else if((i-j==(n-1)/2 || j-i==(n-1)/2) && i != n-1 && j != n-1 && n%2 == 1)
                    {
                        MoveToEx(hdc, nx[i], ny[i], NULL);
                        polyline(nx[i], ny[i], nx[j], ny[j]);
                    }
                    else
                    {
                        MoveToEx(hdc, nx[i], ny[i], NULL);
                        LineTo(hdc, nx[j], ny[j]);
                        TextOut(hdc, xMiddle, yMiddle, str, 3);
                    }
                }
            }
        }

        //Draw vertices
        SelectObject(hdc, BPen);
        for(int i = 0; i<n; i++)
        {
            Ellipse(hdc, nx[i]-dx,ny[i]-dy,nx[i]+dx,ny[i]+dy);
            TextOut(hdc, nx[i]-dtx,ny[i]-dy/2, nn[i], 2);
        }

        TextOut(hdc, offsetX-4*dx, offsetY, "135", 3);
        TextOut(hdc, offsetX, offsetY-3*dy, "25", 3);
        TextOut(hdc, offsetX+3*dx, offsetY, "138", 3);

        MST_Kruskal(Wt);
        printf("Minimum spanning tree weight: %d\n", totalWeight);
        printf("MST matrix: \n");
        printMatrix(MSTMatrix);

        EndPaint(hWnd, &ps);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    default:
        return(DefWindowProc(hWnd, messg, wParam, lParam));
    }
    return 0;
}
